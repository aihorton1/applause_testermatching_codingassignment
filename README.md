#Applause Coding Assignment - Tester Matching
#####Alexander Horton

##Overview
This is my submission for the Applause Coding Assignment as part of the application process.
This is a command line application written in Java 11 which uses the [SQLiteJDBC package](https://bitbucket.org/xerial/sqlite-jdbc).
This application allows the user to import the information from csv files located in the csv folder and match testers with the search criteria.

##Usage:
The application **must** be run from the project's main directory. I have compiled an executable jar *Tester.jar* which can be run from the command line with:

```java -jar TesterMatching.jar```

Before data can be queried, it has to be imported into the database with the ```import``` command from within the application.

The commands for the application are below:

###Commands
- help
    - Print all input options
- import [-n]
    - Import the data from the csv files located in the csv folder. Importing with the -n option will delete all existing data within the database before importing.
    - The imported files are: devices.csv, testers.csv, tester_devices.csv, bugs.csv
- search Country="<ALL|Country>" [or Country="<Country>" ...] and Device="<ALL|Device>" [or Device="<Device>" ...]
    - Search the database for testers in the set Country or Countries and with the set Devices(s). Results are ranked in order of experience.
    - Syntax notes:
        - Only a single space is allowed between criteria
        - Capitalization is not ignored
        - The search country or device must be surrounded with ""
        - There must be no space between before or after '='
- exit
    - Exit the application
  
##Compile and run
If you wish to compile and run the program, use the following commands from the project's main directory:

```
javac -d . src\TesterMatching.java
java -cp .;.\lib\sqlite-jdbc-3.27.2.1.jar TesterMatching
```