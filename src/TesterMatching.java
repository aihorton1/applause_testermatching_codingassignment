import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class TesterMatching {

    /**
     * Main method of the class that is run. Does not use any arguments.
     * @param args
     */
    public static void main(String[] args){

        //create the SQLite database file if it doesn't exist
        if(new File("tester.db").exists() == false){
            createDB();
        }

        System.out.println(
            "Welcome to the Tester Matching Application.\n"+
            "Enter your search criteria below and press Enter.\n"+
            "For a full list of all options type 'help'.");

        boolean inUse = true;
        while(inUse){
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
            String[] arr = input.split(" ", 2);
            String command = arr[0];
            String arguments = "";
            if(arr.length > 1) {
                arguments = arr[1];
            }

            switch(command){
                case "help":
                    printUsage();
                    break;
                case "import":
                    importData(arguments.equals("-n"));
                    break;
                case "search":
                    fetchValues(arguments);
                    break;
                case "exit":
                    inUse = false;
                    break;
                default:
                    System.out.println("Command \"" + command + "\" not recognized.");
                    break;
            }
        }
    }

    /**
     * Prints the usage options of the application.
     */
    private static void printUsage(){
        System.out.println(
                "Commands:\n"+
                "   help\n"+
                "       Print all input options\n"+
                "   import [-n]\n"+
                "       Import the data from the csv files located in the csv folder.\n"+
                "       Importing with the -n option will delete all existing data within the database before importing.\n"+
                "   search Country=\"<ALL|Country>\" [or Country=\"<Country>\" ...] and Device=\"<ALL|Device>\" [or Device=\"<Device>\" ...]\n"+
                "       Search the database for testers in the set Country or Countries and with the set Devices(s).\n"+
                "       Results are ranked in order of experience.\n" +
                "       Syntax notes:\n" +
                "           Only a single space is allowed between criteria\n" +
                "           Capitalization is not ignored\n" +
                "           The search country or device must be surrounded with \"\"\n" +
                "           There must be no space between before or after '='\n"+
                "   exit\n"+
                "       Exit the application"
        );
    }

    /**
     * Creates a SQLite database in the current working directory.
     * The Tables created will be Bugs, Devices, Tester_Device, and Testers.
     */
    private static void createDB(){
        String cwd = System.getProperty("user.dir");
        String url = "jdbc:sqlite:"+cwd+"/tester.db";

        try {
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();

            //Create the Devices table.
            String sql =
                "CREATE TABLE IF NOT EXISTS devices (\n"+
                "   deviceId INTEGER PRIMARY KEY,\n"+
                "   description TEXT NOT NULL\n"+
                ");";
            stmt.execute(sql);

            //Create the Testers table.
            sql =
                "CREATE TABLE IF NOT EXISTS testers (\n"+
                "   testerId INTEGER PRIMARY KEY,\n"+
                "   firstName TEXT NOT NULL,\n"+
                "   lastName TEXT NOT NULL,\n"+
                "   country TEXT NOT NULL,\n"+
                "   lastLogin TEXT NOT NULL\n"+
                ");";
            stmt.execute(sql);

            //Create the Tester_Device table.
            sql =
                "CREATE TABLE IF NOT EXISTS tester_device (\n"+
                "   testerId INTEGER\n"+
                "       CONSTRAINT testerId\n"+
                "           REFERENCES testers,\n"+
                "   deviceId INTEGER \n"+
                "       CONSTRAINT deviceId\n"+
                "           REFERENCES devices\n"+
                ");";
            stmt.execute(sql);

            //Create the Bugs table.
            sql =
                "CREATE TABLE IF NOT EXISTS bugs (\n"+
                "   bugId INTEGER PRIMARY KEY,\n"+
                "   deviceId INTEGER\n"+
                "       CONSTRAINT deviceId\n"+
                "           REFERENCES Devices,\n"+
                "   testerId INTEGER\n"+
                "       CONSTRAINT testerId\n"+
                "           REFERENCES Testers"+
                ");";
            stmt.execute(sql);

            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }


    }

    /**
     * Imports the data in the csv files that are located in the csv folder.
     * @param deleteExisting When true, all data is deleted from the database before new data is imported.
     */
    private static void importData(boolean deleteExisting){
        //delete the db if requested
        if(deleteExisting == true){
            new File("tester.db").delete();
            createDB();
            System.out.println("Database has been reset.");
        }

        //import all of the csv files in the csv folder
        importCSVFile(new File("csv/devices.csv"));
        importCSVFile(new File("csv/testers.csv"));
        importCSVFile(new File("csv/tester_device.csv"));
        importCSVFile(new File("csv/bugs.csv"));
        System.out.println("Data imported successfully.");
    }

    /**
     * Imports the data in a .csv file into the table of the same name in the database.
     * The file name needs to match the name of a database table.
     * The column names and types need to match the column names and types in the database table.
     * @param file
     */
    private static void importCSVFile(File file){
        System.out.println("Importing file: "+ file.getName());
        String cwd = System.getProperty("user.dir");
        String url = "jdbc:sqlite:"+cwd+"/tester.db";

        try {

            Connection conn = DriverManager.getConnection(url);

            //get the header info from the file
            BufferedReader reader = new BufferedReader(new FileReader(file.getAbsolutePath()));
            String line = reader.readLine();
            String[] colNames = line.split(",");
            String[] colTypes = new String[colNames.length];
            //remove the quotes surrounding the data
            for(int i = 0; i < colNames.length; i++){
                colNames[i] = colNames[i].substring(1, colNames[i].length()-1);
            }

            //get the column types from the table that will be inserted into
            String tableName = file.getName().substring(0, file.getName().length()-4);
            String sql = "PRAGMA table_info("+tableName+")";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                //set the column type in the colTypes array at the same index as in the colNames array
                //find the index of the column in the colNames array
                for(int i = 0; i < colNames.length; i++){
                    if(colNames[i].equals(rs.getString("name"))){
                        colTypes[i] = rs.getString("type");
                        break;
                    }
                }
            }

            //build the prepared statement
            sql =
                "INSERT OR REPLACE INTO "+ tableName +"("+ String.join(",", colNames) +")\n"+
                "VALUES (?"+ ",?".repeat(colNames.length-1) +")\n";
            PreparedStatement pstmt = conn.prepareStatement(sql);

            //insert the data in the rest of the file
            while((line = reader.readLine())!= null){
                String[] values = line.split(",");
                for(int i = 0; i <  values.length; i++){
                    String value = values[i].substring(1, values[i].length()-1);
                    switch(colTypes[i]){
                        case "INTEGER":
                            pstmt.setInt(i+1, Integer.parseInt(value));
                            break;
                        case "TEXT":
                            pstmt.setString(i+1, value);
                            break;
                    }
                }

                pstmt.executeUpdate();
            }

            conn.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Prints out the search results from a given search String.
     *
     * @param searchString Must be in the format: Country="<ALL|Country>" [or Country="<Country>" ...] and Device="<ALL|Device> [or Device="<Device>" ...]"
     */
    private static void fetchValues(String searchString){
        String cwd = System.getProperty("user.dir");
        String url = "jdbc:sqlite:"+cwd+"/tester.db";

        try{
            Connection conn = DriverManager.getConnection(url);

            //split the seachString into the individual requested criteria
            ArrayList<String> countries = new ArrayList<String>();
            ArrayList<String> devices = new ArrayList<String>();
            boolean allCountries = false;
            boolean allDevices = false;

            String[] criteria = searchString.split(" and | or ");
            for(String part : criteria){
                String[] parts = part.split("=");
                String category = parts[0];
                String search = parts[1].substring(1, parts[1].length()-1);
                switch (category){
                    case "Country":
                        if(search.equals("ALL")){
                            allCountries = true;
                        }
                        else {
                            countries.add(search);
                        }
                        break;
                    case "Device":
                        if(search.equals("ALL")){
                            allDevices = true;
                        }
                        else {
                            devices.add(search);
                        }
                        break;
                }
            }

            //create the sql query that will collect the tester information
            String sql =
                "SELECT t.firstName, t.lastName, COUNT(bugId) AS filed\n" +
                "FROM\n" +
                "   bugs b\n" +
                "   JOIN devices d on b.deviceId = d.deviceId\n" +
                "   JOIN testers t on b.testerId = t.testerId\n" +
                "   WHERE\n";
            //include all of the requested countries
            if(allCountries == false){
                sql += "country in (?"+ ",?".repeat(countries.size()-1) +")\n";
            }
            //include all of the requested devices
            if(allDevices == false){
                if(allCountries == false){
                    sql += "AND ";
                }
                sql += "description in (?"+ ",?".repeat(devices.size()-1) +")\n";
            }

            sql +=
                "GROUP BY t.testerId\n" +
                "ORDER BY COUNT(bugId) desc";


            //create the prepared statement and set the parameters for all of the search criteria
            PreparedStatement pstmt = conn.prepareStatement(sql);
            for(int i = 0; i < countries.size(); i++){
                pstmt.setString(i+1, countries.get(i));
            }
            for(int i = 0; i < devices.size(); i++){
                pstmt.setString(countries.size()+i+1, devices.get(i));
            }

            //print out the results of the query
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){
                System.out.println(
                        "Tester: "+
                        rs.getString("firstName")+
                        " "+rs.getString("lastName")+
                        ", Bugs filed: "+
                        rs.getString("filed"));
            }

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e){
            System.out.println("Search query syntax incorrect. Please check your syntax.");
        }
    }
}
